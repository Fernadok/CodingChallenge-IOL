﻿using CodingChallenge.Library.Enums;

namespace CodingChallenge.Library.ViewModels
{
    /// <summary>
    /// Clase para devolver el set de datos de la factoria.
    /// </summary>
    public class FiguraViewModel
    {
        /// <summary>
        /// Figura geométrica.
        /// </summary>
        public Figura Figura { get; set; }

        /// <summary>
        /// Cantidad de figuras de cada figura.
        /// </summary>
        public int Cantidad { get; set; }

        /// <summary>
        /// Área total de grupo de figuras.
        /// </summary>
        public decimal AreaTotal { get; set; }

        /// <summary>
        /// Perimetro total de grupo de figuras.
        /// </summary>
        public decimal PerimetroTotal { get; set; }
    }
}
