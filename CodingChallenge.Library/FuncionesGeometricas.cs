﻿using CodingChallenge.Library.Formas.Base;
using CodingChallenge.Library.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace CodingChallenge.Library
{
    /// <summary>
    /// Clase que se encarga de realizar operaciónes sobre figuras geométricas.
    /// </summary>
    public class FuncionesGeometricas
    {
        private List<FormaGeometrica> Figuras = new List<FormaGeometrica>();

        public FuncionesGeometricas(List<FormaGeometrica> parfiguras)
        {
            Figuras = parfiguras;
        }

        /// <summary>
        /// Función que calcula el área y perimetro de una figura.
        /// </summary>
        /// <returns>Retorna el ViewModel con los datos procesados en cada figura (área y perimetro). </returns>
        public List<FiguraViewModel> Calcular()
        {
            //NOTA: Esta iteración podria evitarse ejecutando dichas funciones en el CTOR de cada figura.
            foreach (var fg in Figuras)
            {
                fg.CalcularArea();
                fg.CalcularPerimetro();
            }

            var resultado = Figuras
                            .GroupBy(g => g.Figura)
                            .Select(s => new FiguraViewModel
                            {
                                Figura = s.Select(x => x.Figura).FirstOrDefault(),
                                Cantidad = s.Count(),
                                AreaTotal = s.Sum(x => x.Area),
                                PerimetroTotal = s.Sum(x => x.Perimetro),
                            }).ToList();

            return resultado;
        }
    }
}
