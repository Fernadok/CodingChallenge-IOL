﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;
using CodingChallenge.Library.Resources;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;

namespace CodingChallenge.Library
{
    public class FiguraGeometrica
    {
        /// <summary>
        /// Método para la impresión de reporte de figuras.
        /// </summary>
        /// <param name="formas">Lista de formas geométricas.</param>
        /// <param name="idioma">Idioma en que se va a devolver el reporte.</param>
        /// <returns></returns>
        public static string Imprimir(List<FormaGeometrica> formas, Idioma idioma)
        {
            SetIdioma(idioma);

            FuncionesGeometricas ProcesarFigura = new FuncionesGeometricas(formas);
            var resultado = ProcesarFigura.Calcular();
          
            var sb = new StringBuilder();
           
            // HEADER
            if (!resultado.Any())
            {
                sb.Append($"<h1>{StringResources.EmptyLlist}</h1>");
            }
            else
            {
                sb.Append($"<h1>{StringResources.ShapesReport}</h1>");

                //BODY
                foreach (var item in resultado)
                {
                    sb.Append(ObtenerLinea(item.Cantidad, item.AreaTotal, item.PerimetroTotal, item.Figura));
                }

                //FOOTER
                sb.Append("TOTAL:<br/>");
                sb.AppendFormat("{0} {1} ", resultado.Sum(s => s.Cantidad), StringResources.shapes);
                sb.AppendFormat("{0} {1} ", StringResources.Perimeter, (resultado.Sum(s => s.PerimetroTotal)).ToString("#.##"));
                sb.AppendFormat("{0} {1} ", StringResources.Area, (resultado.Sum(s => s.AreaTotal)).ToString("#.##"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Método para la configuración del recurso de idioma.
        /// </summary>
        /// <param name="idioma">Idioma</param>
        private static void SetIdioma(Idioma idioma)
        {
            string name = string.Empty;
            switch (idioma)
            {
                case Idioma.Castellano:
                    name = "es-AR";
                    break;
                case Idioma.Ingles:
                    name = "en-US";
                    break;
                case Idioma.Aleman:
                    name = "de-DE";
                    break;
            }
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(name);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(name);
        }

        private static string ObtenerLinea(int cantidad, decimal area, decimal perimetro, Figura figura)
        {
            return string.Format("{0} {1} | {2} {3} | {4} {5} <br/>"
                                , cantidad
                                , TraducirForma(figura, cantidad)
                                , StringResources.Area
                                , area.ToString("#.##")
                                , StringResources.Perimeter
                                , perimetro.ToString("#.##"));
        }

        /// <summary>
        /// Método que se encarga de traducir el texto de la forma en el reporte.
        /// </summary>
        /// <param name="figura">Figura</param>
        /// <param name="cantidad">Cantidad</param>
        /// <returns>Nombre de la figura traducida.</returns>
        private static string TraducirForma(Figura figura, int cantidad)
        {
            string forma = figura.ToString();
            if (cantidad > 1)
                forma += "Plural";

            return StringResources.ResourceManager.GetString(forma);
        }
    }
}
