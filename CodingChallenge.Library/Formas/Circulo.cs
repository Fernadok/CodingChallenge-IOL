﻿using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;
using System;

namespace CodingChallenge.Library.Formas
{
    /// <summary>
    /// Clase que representa una forma geométrica.
    /// </summary>
    public class Circulo : FormaGeometrica
    {
        /// <summary>
        /// Lado del círculo.
        /// </summary>
        private decimal Lado { get; set; }

        #region override
        public override decimal Area { get; set; }
        public override decimal Perimetro { get; set; }

        public override Figura Figura => Figura.Circle;

        public Circulo(decimal parLado)
        {
            Lado = parLado;
        }

        public override decimal CalcularArea()
        {
            Area = (decimal)Math.PI * (Lado / 2) * (Lado / 2);
            return Area;
        }

        public override decimal CalcularPerimetro()
        {
            Perimetro = (decimal)Math.PI * Lado;
            return Perimetro;
        }
        #endregion
    }
}
