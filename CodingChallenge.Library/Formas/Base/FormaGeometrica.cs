﻿using CodingChallenge.Library.Enums;

namespace CodingChallenge.Library.Formas.Base
{
    /// <summary>
    /// Clase abstracta para ser heredada por las clases que representen figuras geométricas.
    /// </summary>
    public abstract class FormaGeometrica
    {
        /// <summary>
        /// Área de la figura.
        /// </summary>
        public abstract decimal Area { get; set; }

        /// <summary>
        /// Perimetro de la figura.
        /// </summary>
        public abstract decimal Perimetro { get; set; }

        /// <summary>
        /// Representa un tipo de forma geométrica.
        /// </summary>
        public abstract Figura Figura { get; }

        /// <summary>
        /// Función para calcular el área de una figura geométricas.
        /// </summary>
        /// <returns>Área de una figura.</returns>
        public abstract decimal CalcularArea();

        /// <summary>
        /// Función para calcular el perimetro de una figura geométricas.
        /// </summary>
        /// <returns>Perimetro de una figura.</returns>
        public abstract decimal CalcularPerimetro();
    }
}
