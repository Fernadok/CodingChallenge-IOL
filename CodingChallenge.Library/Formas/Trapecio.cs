﻿using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;

namespace CodingChallenge.Library.Formas
{
    /// <summary>
    /// Clase que representa una forma geométrica.
    /// </summary>
    public class Trapecio : FormaGeometrica
    {
        /// <summary>
        /// Lado mayor del trapecio.
        /// </summary>
        private decimal LadoMayor { get; set; }

        /// <summary>
        /// Lado menor del trapecio.
        /// </summary>
        private decimal LadoMenor { get; set; }

        /// <summary>
        /// Altura del trapecio.
        /// </summary>
        private decimal Alto { get; set; }

        #region override
        public override decimal Area { get; set; }
        public override decimal Perimetro { get; set; }

        public override Figura Figura => Figura.Trapeze;

        public Trapecio(decimal parLadoMayor, decimal parLadoMenor, decimal parAltura)
        {
            LadoMayor = parLadoMayor;
            LadoMenor = parLadoMenor;
            Alto = parAltura;
        }

        public override decimal CalcularArea()
        {
            Area = ((LadoMayor + LadoMenor) / 2) * Alto;
            return Area;
        }

        public override decimal CalcularPerimetro()
        {
            Perimetro = (2 * Alto) + LadoMenor + LadoMayor;
            return Perimetro;
        }
        #endregion
    }
}
