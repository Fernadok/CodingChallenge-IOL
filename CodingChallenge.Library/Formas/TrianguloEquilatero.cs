﻿using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;
using System;

namespace CodingChallenge.Library.Formas
{
    /// <summary>
    /// Clase que representa una forma geométrica.
    /// </summary>
    public class TrianguloEquilatero : FormaGeometrica
    {
        /// <summary>
        /// Lado del triángulo.
        /// </summary>
        private decimal Lado { get; set; }

        #region override
        public override decimal Area { get; set; }
        public override decimal Perimetro { get; set; }

        public override Figura Figura => Figura.EquilateralTriangle;

        public TrianguloEquilatero(decimal parLado)
        {
            Lado = parLado;
        }

        public override decimal CalcularArea()
        {
            Area = ((decimal)Math.Sqrt(3) / 4) * Lado * Lado;
            return Area;
        }

        public override decimal CalcularPerimetro()
        {
            Perimetro = Lado * 3;
            return Perimetro;
        }
        #endregion
    }
}
