﻿using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;

namespace CodingChallenge.Library.Formas
{
    /// <summary>
    /// Clase que representa una forma geométrica.
    /// </summary>
    public class Rectangulo : FormaGeometrica
    { 
        /// <summary>
        /// Base del rectángulo.
        /// </summary>
        private decimal Lado { get; set; }

        /// <summary>
        /// Alto del triángulo.
        /// </summary>
        private decimal Alto { get; set; }

        #region override
        public override decimal Area { get; set; }
        public override decimal Perimetro { get; set; }

        public override Figura Figura => Figura.Rectangle;

        public Rectangulo(decimal parLado, decimal parAlto)
        {
            Lado = parLado;
            Alto = parAlto;
        }

        public override decimal CalcularArea()
        {
            Area = Lado * Alto;
            return Area;
        }

        public override decimal CalcularPerimetro()
        {
            Perimetro = (Lado * 2) + (Alto * 2);
            return Perimetro;
        }
        #endregion
    }
}
