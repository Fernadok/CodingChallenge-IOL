﻿using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas.Base;

namespace CodingChallenge.Library.Formas
{
    /// <summary>
    /// Clase que representa una forma geométrica.
    /// </summary>
    public class Cuadrado : FormaGeometrica
    {
        /// <summary>
        /// Lado del cuadrado.
        /// </summary>
        private decimal Lado { get; set; }

        #region override
        public override decimal Area { get; set; }
        public override decimal Perimetro { get; set; }

        public override Figura Figura => Figura.Square;

        public Cuadrado(decimal parLado)
        {
            Lado = parLado;
        }

        public override decimal CalcularArea()
        {
            Area = Lado * Lado;
            return Area;
        }

        public override decimal CalcularPerimetro()
        {
            Perimetro = Lado * 4;
            return Perimetro;
        }
        #endregion
    }
}
