﻿namespace CodingChallenge.Library.Enums
{
    /// <summary>
    /// Enumerador para seteo de idioma.
    /// </summary>
    public enum Idioma
    {
        Castellano  = 1,
        Ingles      = 2,
        Aleman      = 3
    }
}
