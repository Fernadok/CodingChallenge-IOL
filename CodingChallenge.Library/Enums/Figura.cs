﻿namespace CodingChallenge.Library.Enums
{
    /// <summary>
    /// Enumerador que representa algún tipo de figura geométrica.
    /// </summary>
    public enum Figura
    {
        Square              = 1,
        EquilateralTriangle = 2,
        Circle              = 3,
        Trapeze             = 4,
        Rectangle           = 5
    }
}
