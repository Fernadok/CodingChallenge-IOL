﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodingChallenge.Library.Enums;
using CodingChallenge.Library.Formas;
using CodingChallenge.Library.Formas.Base;
using System.Collections.Generic;

namespace CodingChallenge.Library.Tests
{
    [TestClass]
    public class LibraryTest
    {
        [TestMethod]
        public void TestResumenVariasFormasIngles()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(5),
                new Cuadrado(5),
                new TrianguloEquilatero(3)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Ingles);
            Assert.AreEqual("<h1>Shapes report</h1>2 squares | Area 50 | Perimeter 40 <br/>1 equilateral triangle | Area 3,9 | Perimeter 9 <br/>TOTAL:<br/>3 shapes Perimeter 49 Area 53,9 ", resume);
        }

        [TestMethod]
        public void TestResumenVariasFormasCastellano()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(5),
                new Rectangulo(5,2),
                new TrianguloEquilatero(3)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Castellano);
            Assert.AreEqual("<h1>Reporte de Formas</h1>1 cuadrado | Area 25 | Perimetro 20 <br/>1 rectángulo | Area 10 | Perimetro 14 <br/>1 triángulo equilátero | Area 3,9 | Perimetro 9 <br/>TOTAL:<br/>3 formas Perimetro 43 Area 38,9 ", resume);
        }

        [TestMethod]
        public void TestResumenVariasFormasAleman()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(5),
                new Rectangulo(5,2),
                new Circulo(3)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Aleman);
            Assert.AreEqual("<h1>Formen berichten</h1>1 platz | Bereich 25 | Umfang 20 <br/>1 rechteck | Bereich 10 | Umfang 14 <br/>1 kreis | Bereich 7,07 | Umfang 9,42 <br/>TOTAL:<br/>3 formen Umfang 43,42 Bereich 42,07 ", resume);
        }

        [TestMethod]
        public void TestResumenListaVaciaIngles()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica>();

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Ingles);
            Assert.AreEqual("<h1>Empty list of shapes!</h1>", resume);
        }

        [TestMethod]
        public void TestResumenListaVaciaCastellano()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica>();

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Castellano);
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>", resume);
        }

        [TestMethod]
        public void TestResumenListaVaciaAleman()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica>();

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Aleman);
            Assert.AreEqual("<h1>Leere Liste der Formen!</h1>", resume);
        }

        [TestMethod]
        public void TestResumenTodasLasFormasAleman()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(10),
                new Rectangulo(10,8),
                new Circulo(4.5m),
                new TrianguloEquilatero(22),
                new Trapecio(12,10,6)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Aleman);
            Assert.AreEqual("<h1>Formen berichten</h1>1 platz | Bereich 100 | Umfang 40 <br/>1 rechteck | Bereich 80 | Umfang 36 <br/>1 kreis | Bereich 15,9 | Umfang 14,14 <br/>1 gleichseitiges Dreieck | Bereich 209,58 | Umfang 66 <br/>1 Trapez | Bereich 66 | Umfang 34 <br/>TOTAL:<br/>5 formen Umfang 190,14 Bereich 471,48 ", resume);
        }

        [TestMethod]
        public void TestResumenTodasLasFormasIngles()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(3),
                new Rectangulo(4,7),
                new Circulo(8.5m),
                new TrianguloEquilatero(18),
                new Trapecio(22,15,9)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Ingles);
            Assert.AreEqual("<h1>Shapes report</h1>1 square | Area 9 | Perimeter 12 <br/>1 rectangle | Area 28 | Perimeter 22 <br/>1 circle | Area 56,75 | Perimeter 26,7 <br/>1 equilateral triangle | Area 140,3 | Perimeter 54 <br/>1 trapeze | Area 166,5 | Perimeter 55 <br/>TOTAL:<br/>5 shapes Perimeter 169,7 Area 400,54 ", resume);
        }

        [TestMethod]
        public void TestResumenTodasLasFormasCastellano()
        {
            List<FormaGeometrica> lista = new List<FormaGeometrica> {
                new Cuadrado(11),
                new Rectangulo(6,10),
                new Circulo(11.2m),
                new TrianguloEquilatero(33),
                new Trapecio(31,20,19)
            };

            var resume = FiguraGeometrica.Imprimir(lista, Idioma.Castellano);
            Assert.AreEqual("<h1>Reporte de Formas</h1>1 cuadrado | Area 121 | Perimetro 44 <br/>1 rectángulo | Area 60 | Perimetro 32 <br/>1 círculo | Area 98,52 | Perimetro 35,19 <br/>1 triángulo equilátero | Area 471,55 | Perimetro 99 <br/>1 trapecio | Area 484,5 | Perimetro 89 <br/>TOTAL:<br/>5 formas Perimetro 299,19 Area 1235,57 ", resume);
        }
    }
}
